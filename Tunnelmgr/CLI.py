# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnelmanager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import getopt
import sys

from Tunnelmgr import Localization
from Tunnelmgr import PortInfo
from Tunnelmgr.Controller import Controller
from Tunnelmgr.TunnelInfo import TunnelInfo
from Tunnelmgr.KeyInfo import KeyInfo
from Tunnelmgr.ErrorReporter import ErrorReporter
from Tunnelmgr.Config import Configuration


class Application(object):
    """An object class to represent the command line interface."""

    def __get_exit_code(self):
        return self.__m_exitCode

    exitCode = property(__get_exit_code)

    def __init__(self, args):
        """The object constructor."""
        Localization.initialize('CLI', '')
        self.portList = []
        self.keyList = []
        self.__m_exitCode = 0

        if not self.parse_opts(args):
            return

        self.errorReporter = ErrorReporter()
        self.config = Configuration(self.properties)
        self.controller = Controller(self.errorReporter,
                                     self.config,
                                     self.readFile)

        if self.command == "list-t":
            self.list_tunnels()
        elif self.command == "add-t":
            self.add_tunnel()
        elif self.command == "update-t":
            self.update_tunnel()
        elif self.command == "delete-t":
            self.delete_tunnel()
        elif self.command == "delete-p":
            self.delete_port()
        elif self.command == "list-k":
            self.list_keys()
        elif self.command == "add-k":
            self.add_keys()
        elif self.command == "delete-k":
            self.delete_keys()
        elif self.command == "load-k":
            self.load_keys()
        elif self.command == "unload-k":
            self.controller.unload_keys()
        else:
            print("Unrecognized command: %s" % self.command, file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2

    def usage(self):
        """Output usage information"""
        print("""\
%(cmd)s: [-h] [-f %(file)s] [-P %(file)s ] -c %(command)s [%(opt)s]
\t-h|--help       -- %(help)s
\t-f|--file       -- %(conf)s
\t-P|--properties -- %(props)s
\t-c|--command    -- %(exec)s
\t-n|--name       -- %(name)s
\t-u|--userid     -- %(userid)s
\t-H|--host       -- %(host)s
\t-p|--port       -- %(port)s
\t-k|--key        -- %(keyid)s
\t-L|--local      -- %(local)s
\t-R|--remote     -- %(remote)s
\t-D|--dynamic    -- %(dynamic)s
\t-K|--keyfile    -- %(keyfile)s
""" % {'cmd': sys.argv[0], 'file': "file", 'command': "command", 'opt': "options", 'help': "output this message",
       'conf': "config file to load", 'props': "properties file to load", 'exec': "command to execute",
       'name': "tunnel name", 'userid': "user id", 'host': "tunnel host", 'port': "tunnel port", 'keyid': "key ID",
       'local': "local port forward", 'remote': "remote port forward", 'dynamic': "dynamic SOCKS proxy port",
       'keyfile': "SSH private key filename"}, file=sys.stderr)

    def parse_opts(self, args):
        """Parse the command line options."""
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "hf:c:n:H:p:k:L:R:D:K:N:P:u:", [
                                           "help", "file=", "command=",
                                           "name=", "host=", "port=", "key=",
                                           "local=", "remote=", "dynamic=",
                                           "keyfile=", "newname=",
                                           "properties=", "userid="])
        except getopt.GetoptError as e:
            print(str(e), file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return False

        if len(args) > 0:
            print("Extra arguments: ", args, file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return False

        self.readFile = None
        self.command = None
        self.tunnel = None
        self.newName = None
        self.tunnelHost = None
        self.tunnelPort = None
        self.requiredKey = None
        self.properties = None

        for o, a in opts:
            if o in ("-h", "--help"):
                self.usage()
                self.__m_exitCode = 0
                return False
            elif o in ("-f", "--file"):
                self.readFile = a
            elif o in ("-c", "--command"):
                self.command = a
            elif o in ("-n", "--name"):
                self.tunnel = a
            elif o in ("-u", "--userid"):
                self.userid = a
            elif o in ("-H", "--host"):
                self.tunnelHost = a
            elif o in ("-p", "--port"):
                self.tunnelPort = a
            elif o in ("-k", "--key"):
                self.requiredKey = a
            elif o in ("-L", "--local"):
                if not self.parse_port(PortInfo.LOCAL_TYPE, a):
                    return False
            elif o in ("-R", "--remote"):
                if not self.parse_port(PortInfo.REMOTE_TYPE, a):
                    return False
            elif o in ("-D", "--dynamic"):
                if not self.parse_port(PortInfo.DYNAMIC_TYPE, a):
                    return False
            elif o in ("-K", "--keyfile"):
                self.keyList.append(a)
            elif o in ("-N", "--newname"):
                self.newName = a
            elif o in ("-P", "--properties"):
                self.properties = a
            else:
                print("Unhandled option: ", o, file=sys.stderr)
                self.usage()
                self.__m_exitCode = 2
                return False

        if not self.command:
            print("No command specified.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return False

        return True

    def parse_port(self, type, arg):
        """Parse port string and add to list."""
        fields = arg.split(':')
        new_port = None
        if type in (PortInfo.LOCAL_TYPE, PortInfo.REMOTE_TYPE):
            if len(fields) == 3:
                new_port = PortInfo.ForwardPort(
                    type, "", "", fields[0], fields[1], fields[2])
            elif len(fields) == 4:
                new_port = PortInfo.ForwardPort(
                    type, "", fields[0], fields[1], fields[2], fields[3])
            elif len(fields) == 5:
                new_port = PortInfo.ForwardPort(
                    type, fields[0], fields[1], fields[2], fields[3], fields[4])
            else:
                print("Invalid %(type)s fwd port spec: %(opt)s" % {
                    'type': ("local" if type == PortInfo.LOCAL_TYPE else "remote"), 'opt': arg}, file=sys.stderr)
                self.__m_exitCode = 2
                self.usage()
                return False
            self.portList.append(new_port)
        else:
            if len(fields) == 1:
                new_port = PortInfo.DynamicPort("", "", fields[0])
            elif len(fields) == 2:
                new_port = PortInfo.DynamicPort("", fields[0], fields[1])
            elif len(fields) == 3:
                new_port = PortInfo.DynamicPort(fields[0], fields[1], fields[2])
            else:
                print("Invalid dynamic port spec: %s" % arg, file=sys.stderr)
                self.__m_exitCode = 2
                self.usage()
                return False
            self.portList.append(new_port)

        return True

    def list_tunnels(self):
        """List tunnels."""
        if self.tunnel:
            to_list = self.controller.lookup_tunnel(self.tunnel)
            if to_list:
                self.print_tunnel(to_list)
            else:
                print("Tunnel %s does not exist" % self.tunnel, file=sys.stderr)
                self.__m_exitCode = 1
        elif len(self.controller.tunnelList) == 0:
            print("No Tunnels defined in %s" % self.controller.filename, file=sys.stderr)
        else:
            for tunnel in self.controller.tunnelList:
                self.print_tunnel(tunnel)

    def print_tunnel(self, tunnel):
        """Print tunnel details."""
        print(tunnel)
        for port in tunnel.portList:
            print("\t%s" % port)

    def add_tunnel(self):
        """Add tunnel."""
        if not self.tunnel:
            print("No tunnel name specified.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return
        elif self.controller.lookup_tunnel(self.tunnel):
            print("Specified tunnel exists: %s" % self.tunnel, file=sys.stderr)
            self.__m_exitCode = 1
            return
        elif not self.tunnelHost:
            print("No tunnel host specified for %s" % self.tunnel, file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return
        elif len(self.portList) == 0:
            print("No ports specified for %s" % self.tunnel, file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return

        new_tunnel = TunnelInfo(self.tunnel, self.userid, self.tunnelHost,
                                self.tunnelPort if self.tunnelPort else "22",
                                self.requiredKey, self.errorReporter,
                                self.controller)
        for port in self.portList:
            new_tunnel.add_port(port)

        self.controller.insert_tunnel(new_tunnel)
        self.controller.save_file(self.readFile)

    def update_tunnel(self):
        """Update tunnel."""
        if not self.tunnel:
            print("No tunnel name specified.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return

        existing_tunnel = self.controller.lookup_tunnel(self.tunnel)
        if not existing_tunnel:
            print("Specified tunnel does not exist: %s" % self.tunnel, file=sys.stderr)
            self.__m_exitCode = 1
            return

        if self.newName:
            existing_tunnel.name = self.newName
        if self.tunnelHost:
            existing_tunnel.tunnelHost = self.tunnelHost
        if self.tunnelPort:
            existing_tunnel.tunnelPort = self.tunnelPort
        if self.requiredKey:
            existing_tunnel.requiredKey = self.requiredKey
        for port in self.portList:
            existing_tunnel.add_port(port)

        self.controller.save_file(self.readFile)

    def delete_tunnel(self):
        """Delete tunnel."""
        if not self.tunnel:
            print("No tunnel name specified.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return

        existing_tunnel = self.controller.lookup_tunnel(self.tunnel)
        if not existing_tunnel:
            print("Specified tunnel does not exist: %s" % self.tunnel, file=sys.stderr)
            self.__m_exitCode = 1
            return

        self.controller.remove_tunnel(existing_tunnel)
        self.controller.save_file(self.readFile)

    def delete_port(self):
        """Delete port from tunnel."""
        if not self.tunnel:
            print("No tunnel name specified.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
            return

        existing_tunnel = self.controller.lookup_tunnel(self.tunnel)
        if not existing_tunnel:
            print("Specified tunnel does not exist: %s" % self.tunnel, file=sys.stderr)
            self.__m_exitCode = 1
            return

        for delPort in self.portList:
            found_match = False
            for existingPort in existing_tunnel.portList:
                if existingPort.is_match(delPort):
                    found_match = True
                    existing_tunnel.remove_port(existingPort)
                    break
            if not found_match:
                print("No match for port in tunnel %(tun)s: %(port)s" % {
                    'tun': self.tunnel, 'port': delPort}, file=sys.stderr)
                self.__m_exitCode = 1
                return

        self.controller.save_file(self.readFile)

    def list_keys(self):
        """List keys."""
        if len(self.controller.keyList) == 0:
            print("No keys defined in %s" % self.controller.filename, file=sys.stderr)
        else:
            for key in self.controller.keyList:
                print(key)

    def add_keys(self):
        """Add keys."""
        if len(self.keyList) == 0:
            print("No keys specified to add.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 2
        for key in self.keyList:
            if self.controller.lookup_key(key) is not None:
                print("Specified key exists: %s" % key, file=sys.stderr)
                self.__m_errorCode = 1
                return
            new_key = KeyInfo(key)
            self.controller.insert_key(new_key)
        self.controller.save_file(None)

    def delete_keys(self):
        """Delete keys."""
        if len(self.keyList) == 0:
            print("No keys specified to delete.", file=sys.stderr)
            self.usage()
            self.__m_exitCode = 1
        for key in self.keyList:
            to_del = self.controller.lookup_key(key)
            if not to_del:
                print("Specified key does not exist: %s" % key, file=sys.stderr)
                self.__m_exitCode = 1
                return
            self.controller.remove_key(to_del)
        self.controller.save_file(None)

    def load_keys(self):
        """Load all keys."""
        process = self.controller.start_load_keys()
        self.controller.check_load_keys_complete(process, True)
