# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnelmanager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

APP_INDICATOR_AVAILABLE = True
try:
    import appindicator
except:
    APP_INDICATOR_AVAILABLE = False
    
APP_INDICATOR_AVAILABLE = False


class StatusIcon(object):
    """This class represents the status box icon."""

    def __init__(self, gui):
        """The object constructor."""
        self.gui = gui
        self.menu = None
        self.icon = None
        self.indicator = None

    def is_enabled(self):
        """Is the status icon enabled?"""
        return self.menu is not None

    def display_menu(self, widget):
        """Display the menu for the StatusIcon"""
        if not APP_INDICATOR_AVAILABLE:
            self.menu.popup(None, None, None, 1, 0, Gtk.get_current_event_time())

    def disable(self):
        """Disable the status icon"""
        if self.icon is not None:
            self.icon.set_visible(False)
        self.menu = None
        self.indicator = None
        if self.gui.mainWindow is None:
            self.gui.build_window()

    def enable(self):
        """Enable the status icon"""
        if self.menu is None:
            self.menu = Gtk.Menu()
            if APP_INDICATOR_AVAILABLE:
                self.indicator = appindicator.Indicator(("SSH Tunnel Manager"),
                                                        "network-wired", appindicator.CATEGORY_OTHER)
                self.indicator.set_status(appindicator.STATUS_ACTIVE)
                self.indicator.set_menu(self.menu)
            elif self.icon is not None:
                self.icon.set_visible(True)
            else:
                self.icon = Gtk.StatusIcon()
                self.icon.set_from_stock(Gtk.STOCK_NETWORK)
                self.icon.set_tooltip_text("SSH Tunnel Manager")
                self.icon.connect('activate', self.display_menu)
            self.reload_menu()

    def reload_menu(self):
        """Dynamically reload the popup menu."""

        if self.menu is None:
            return

        if APP_INDICATOR_AVAILABLE:
            self.menu = Gtk.Menu()
        else:
            self.menu.foreach(self.menu.remove)

        key_menu = Gtk.Menu()
        self.add_to_menu(key_menu, "Load ", Gtk.STOCK_CONNECT,
                         self.gui.on_load_keys)
        self.add_to_menu(key_menu, "Unload", Gtk.STOCK_DISCONNECT,
                         self.gui.on_unload_keys)
        self.add_to_menu(key_menu, "View Agent", Gtk.STOCK_INFO,
                         self.gui.on_view_agent)

        keys_item = self.add_to_menu(self.menu, 'Keys',
                                     Gtk.STOCK_DIALOG_AUTHENTICATION)
        keys_item.set_submenu(key_menu)

        separator = Gtk.SeparatorMenuItem()
        self.menu.append(separator)
        separator.show()

        for tunnel in self.gui.controller.tunnelList:
            if tunnel.isActive:
                self.add_to_menu(self.menu, tunnel.name, Gtk.STOCK_YES,
                                 self.gui.stop_tunnel, tunnel)
            else:
                self.add_to_menu(self.menu, tunnel.name, Gtk.STOCK_NO,
                                 self.gui.start_tunnel, tunnel)

        separator = Gtk.SeparatorMenuItem()
        self.menu.append(separator)
        separator.show()

        if self.gui.mainWindow is None:  # or
            # TODO: re-add self.gui.mainWindow.iconify_initially):
            self.add_to_menu(self.menu, None, Gtk.STOCK_EDIT,
                             self.toggle_hide)
        self.add_to_menu(self.menu, None, Gtk.STOCK_PREFERENCES,
                         self.gui.on_preferences)
        self.add_to_menu(self.menu, None, Gtk.STOCK_QUIT,
                         self.gui.on_quit)

        if (APP_INDICATOR_AVAILABLE):
            self.indicator.set_menu(self.menu)

    def add_to_menu(self, menu, label, stock_id, handler=None, data=None):
        """Add an item to the menu."""
        if stock_id is None:
            item = Gtk.MenuItem(label)
        elif stock_id == Gtk.STOCK_YES or stock_id == Gtk.STOCK_NO:
            item = Gtk.CheckMenuItem(label)
            item.set_active(stock_id == Gtk.STOCK_YES)
        elif label is None:
            item = Gtk.ImageMenuItem(stock_id)
        else:
            item = Gtk.ImageMenuItem(label)
            image = Gtk.Image.new_from_stock(stock_id, Gtk.IconSize.MENU)
            item.set_image(image)
        if handler:
            if (data):
                item.connect('activate', handler, data)
            else:
                item.connect('activate', handler)
        menu.append(item)
        item.show()
        return item

    def toggle_hide(self, widget):
        """Unhide the tunnel manager, if it is hidden."""
        if self.gui.mainWindow is not None:
            if self.gui.mainWindow.iconify_initially:
                self.gui.mainWindow.deiconify()
                self.gui.mainWindow.present()
            elif (self.gui.config.safe_get_boolean('GUI', 'showIcon') and
                  self.gui.config.safe_get_boolean('GUI', 'minToTray')):
                self.gui.destroy_window()
            else:
                self.gui.mainWindow.iconify()
        else:
            self.gui.build_window()
        self.reload_menu()
