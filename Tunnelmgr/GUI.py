__author__ = "Brandon Williams\n<opensource@subakutty.net>"
__version__ = "0.0.0.2, based on TunnelManager 0.7.7.4"
__date__ = "Date: 2020/04/12"
__copyright__ = "Copyright (c) 2008-2014 Brandon Williams\n 2020 Andreas Kremser"
__license__ = "\
This program is free software; you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License version 2 as\n\
published by the Free Software Foundation.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License along\n\
with this program; if not, write to the Free Software Foundation, Inc.,\n\
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA, or view\n\
the following: http://www.subakutty.net/tunnelmgr/LICENSE"

import gi
gi.require_version("Gtk", "3.0")
import os
import sys
import getopt
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject

gu = Gtk.Builder()
import subprocess

from Tunnelmgr import Localization
from Tunnelmgr import PortInfo
from Tunnelmgr.TunnelInfo import TunnelInfo
from Tunnelmgr.KeyInfo import KeyInfo
from Tunnelmgr.GtkErrorReporter import GtkErrorReporter
from Tunnelmgr.Controller import Controller
from Tunnelmgr.StatusIcon import StatusIcon
from Tunnelmgr.Config import Configuration
import pkg_resources

class Application(object):
    """A class to represent the TunnelManager GUI"""

    def __init__(self, args):
        """The object constructor"""
        Localization.initialize('GUI', '/usr/share/locale/')

        (configFile, autoStart, paramFile, gladePath) = self.parse_opts(args)

        self.initialize_configuration(paramFile)
        self.errorReporter = GtkErrorReporter()
        self.controller = Controller(self.errorReporter, self.config, configFile)
        self.do_auto_start(autoStart)
#        self.uiFile = gladePath + "/tunnelmgr.ui"
        self.uiFile = pkg_resources.resource_filename(__name__,'data/tunnelmgr.ui')

        self.filter = Gtk.FileFilter()
        self.filter.set_name("XML files")
        self.filter.add_pattern("*.xml")

        self.quit_on_destroy = False
        self.in_focus = False

        self.mainWindow = None
        self.keyList = None
        self.statusIcon = StatusIcon(self)
        if not self.config.safe_get_boolean('GUI', 'showIcon'):
            self.build_window()
        elif (not (self.config.safe_get_boolean('GUI', 'minToTray') and
                   self.config.safe_get_boolean('GUI', 'launchInTray'))):
            self.build_window()
            self.statusIcon.enable()
        else:
            self.statusIcon.enable()

    def build_window(self):
        """Build the GUI window"""
        self.widgetTree = Gtk.Builder()
        self.widgetTree.add_objects_from_file(self.uiFile, ["MainWindow", "uimanager1"])
        self.widgetTree.connect_signals(self)
        self.mainWindow = self.widgetTree.get_object("MainWindow")
        self.mainWindow.set_focus(None)
        self.yesPixbuf = self.mainWindow.render_icon(Gtk.STOCK_YES, Gtk.IconSize.MENU)
        self.noPixbuf = self.mainWindow.render_icon(Gtk.STOCK_NO, Gtk.IconSize.MENU)
        self.initialize_tunnel_view()
        self.load_tunnel_view()
        self.set_title_from_filename(self.controller.filename)
        self.mainWindow.show()

    def destroy_window(self):
        """Destroy the GUI window"""
        self.mainWindow.destroy()
        self.mainWindow = None
        self.statusIcon.reload_menu()

    def usage(self):
        """Output usage information"""
        print("""\
%(prog)s: [-h] [-a] [-f %(file)s] [-P %(file)s] [-g %(dir)s]
\t-h|--help       -- %(help)s
\t-a|--autostart  -- %(restore)s
\t-f|--file       -- %(conf)s
\t-P|--properties -- %(props)s
\t-g|--gladepath  -- %(glade)s
""" % {'prog': sys.argv[0], 'file': ("file"), 'dir': ("directory"),
       'help': ("output this message"), 'restore': ("restore tunnel state"),
       'conf': ("config file to load"), 'props': ("properties file to load"),
       'glade': ("alternate glade file location")}, file=sys.stderr)

    def parse_opts(self, args):
        """Parse the commandline options."""
        try:
            opts, args = getopt.getopt(sys.argv[1:],
                                       "hag:f:P:",
                                       ["help", "autostart",
                                        "gladepath=", "file=", "properties="])
        except getopt.GetoptError as e:
            print(str(e), file=sys.stderr)
            self.usage()
            sys.exit(2)

        paramFile = None
        configFile = None
        autoStart = None
        import pkg_resources
#        gladePath = pkg_resources.resource_filename('tunnelmgr','tunnelmgr/')
        gladePath = "_invalid_/usr/local/share/tunnelmgr"

        for o, a in opts:
            if o in ("-h", "--help"):
                self.usage()
                sys.exit()
            elif o in ("-a", "--autostart"):
                autoStart = True
            elif o in ("-f", "--file"):
                configFile = a
            elif o in ("-g", "--gladepath"):
                gladePath = a
            elif o in ("-P", "--properties"):
                paramFile = a
            else:
                print("Unhandled option: ", o, file=sys.stderr)
                self.usage()
                sys.exit(2)

        if (len(args) > 0):
            print("Extra arguments: ", args, file=sys.stderr)
            self.usage()
            sys.exit(2)

        return (configFile, autoStart, paramFile, gladePath)

    def initialize_configuration(self, paramFile):
        """Initialize the configuration object and default settings."""
        self.config = Configuration(paramFile)
        self.config.safe_set('DEFAULT', 'autoSave', "%s" % True)
        self.config.safe_set('DEFAULT', 'restoreState', "%s" % True)
        self.config.safe_set('DEFAULT', 'loadkeys', "%s" % True)
        self.config.safe_set('DEFAULT', 'showIcon', "%s" % True)
        self.config.safe_set('DEFAULT', 'minToTray', "%s" % False)
        self.config.safe_set('DEFAULT', 'launchInTray', "%s" % False)

    def do_auto_start(self, autoStart):
        """Handle operations required at autostart."""
        if not autoStart:
            # First, get setting from config file, if not give by command line
            autoStart = self.config.safe_get_boolean('GUI', 'restoreState')
        if not autoStart:
            # Not enabled in config file, either.
            for tunnel in self.controller.tunnelList:
                if tunnel.needs_activate():
                    tunnel.cancel_activate()
        if self.config.safe_get_boolean('GUI', 'loadKeys'):
            self.on_load_keys(None)
            GObject.timeout_add(100, self.wait_for_auto_load)
        elif autoStart:
            self.controller.start_tunnels(self.schedule_tunnel_cleanup, True)
            if self.mainWindow != None:
                self.load_tunnel_view()
            if self.statusIcon.is_enabled():
                self.statusIcon.reload_menu()

    def set_title_from_filename(self, fileName):
        """Set the title of the main window"""
        if not self.mainWindow:
            return
        title = "SSH Tunnel Manager - "
        if fileName:
            title = title + os.path.basename(fileName)
        else:
            title = title + "Untitled"
        self.mainWindow.set_title(title)

    def initialize_tunnel_view(self):
        """Initialize the columns in the tunnel view"""
        self.tunnelView = self.widgetTree.get_object("tunnelView")
        self.add_pixbuf_column(self.tunnelView, ("State"), 1)
        self.add_text_column(self.tunnelView, ("Name"), 2)
        self.add_text_column(self.tunnelView, ("User ID"), 3)
        self.add_text_column(self.tunnelView, ("Tunnel Host"), 4)
        self.add_text_column(self.tunnelView, ("Tunnel Port"), 5)
        self.add_text_column(self.tunnelView, ("Required Key"), 6)
        self.add_text_column(self.tunnelView, ("Type"), 7)
        self.add_text_column(self.tunnelView, ("Name"), 8)
        self.add_text_column(self.tunnelView, ("Bind IP"), 9)
        self.add_text_column(self.tunnelView, ("Local Port"), 10)
        self.add_text_column(self.tunnelView, ("Remote Host"), 11)
        self.add_text_column(self.tunnelView, ("Remote Port"), 12)
        self.tunnelList = Gtk.TreeStore(GObject.TYPE_PYOBJECT,
                                        GdkPixbuf.Pixbuf,
                                        # Gtk.Image().get_pixbuf(),
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING,
                                        GObject.TYPE_STRING)
        self.tunnelView.set_model(self.tunnelList)

    def initialize_port_view(self, portView):
        """Initialize the columns in the port view"""
        self.add_text_column(portView, "Type", 1)
        self.add_text_column(portView, "Name", 2)
        self.add_text_column(portView, "Bind IP", 3)
        self.add_text_column(portView, "Local Port", 4)
        self.add_text_column(portView, "Remote Host", 5)
        self.add_text_column(portView, "Remote Port", 6)
        self.portList = Gtk.ListStore(GObject.TYPE_PYOBJECT,
                                      GObject.TYPE_STRING,
                                      GObject.TYPE_STRING,
                                      GObject.TYPE_STRING,
                                      GObject.TYPE_STRING,
                                      GObject.TYPE_STRING,
                                      GObject.TYPE_STRING)
        portView.set_model(self.portList)

    def initialize_key_view(self, keyView):
        """Initialize the columns in the key view"""
        self.add_text_column(keyView, "File Name", 1)
        self.add_text_column(keyView, "ID", 2)
        self.add_text_column(keyView, "Type", 3)
        self.add_text_column(keyView, "Size", 4)
        self.add_text_column(keyView, "Fingerprint", 5)
        self.keyList = Gtk.ListStore(GObject.TYPE_PYOBJECT,
                                     GObject.TYPE_STRING,
                                     GObject.TYPE_STRING,
                                     GObject.TYPE_STRING,
                                     GObject.TYPE_STRING,
                                     GObject.TYPE_STRING)
        keyView.set_model(self.keyList)
        return keyView

    def add_text_column(self, treeView, title, columnId):
        """Add a text column to tree view"""
        cell_renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title, cell_renderer, text=columnId)
        column.set_sort_column_id(columnId)
        treeView.append_column(column)

    def add_pixbuf_column(self, treeView, title, columnId):
        """Add a pixbuf column to tree view"""
        cell_renderer = Gtk.CellRendererPixbuf()
        column = Gtk.TreeViewColumn(title, cell_renderer, pixbuf=columnId)
        column.set_sort_column_id(columnId)
        treeView.append_column(column)

    def load_key_view(self):
        """Load key view from config controller."""
        self.keyList.clear()
        for key in self.controller.keyList:
            attributes = [key]
            attributes.extend(key.get_attributes())
            self.keyList.append(attributes)

    def load_tunnel_view(self):
        """Load tunnel view from config controller."""
        self.tunnelList.clear()
        for tunnel in self.controller.tunnelList:
            self.add_tunnel_to_tunnel_view(tunnel, self.tunnelList)

    def update_tunnel(self, tunnel):
        """Update the tunnel in the tunnel view."""
        if self.mainWindow is not None:
            iter = self.tunnelList.get_iter_first()
            while iter:
                obj = self.tunnelList.get_value(iter, 0)
                if tunnel.name == obj.name:
                    self.add_tunnel_to_tunnel_view(tunnel, self.tunnelList, iter)
                    break
                iter = self.tunnelList.iter_next(iter)
        if self.statusIcon.is_enabled():
            self.statusIcon.reload_menu()

    def add_tunnel_to_tunnel_view(self, tunnel, list, iter=None):
        """Add/Update tunnel and all ports to list."""
        tunnel_view_list = [tunnel]
        attrs = tunnel.get_attributes()
        if attrs[0]:
            tunnel_view_list.append(self.yesPixbuf)
        else:
            tunnel_view_list.append(self.noPixbuf)
        tunnel_view_list.extend(attrs[1:])
        # Pad to fill columns occupied by port information.
        tunnel_view_list.extend([None, None, None, None, None, None])
        parent = None
        if iter:
            # Update; remove and insert back into same position
            parent = list.insert_before(None, iter, tunnel_view_list)
            list.remove(iter)
        else:
            parent = list.append(None, tunnel_view_list)
        for port in tunnel.portList:
            self.add_port_to_tunnel_view(port, list, parent)

    def add_port_to_tunnel_view(self, port, list, parent):
        """Add port to tunnel view."""
        tunnel_view_list = [port]
        # Pad to fill columns occupied by tunnel information.
        tunnel_view_list.extend([None, None, None, None, None, None])
        tunnel_view_list.extend(port.get_attributes())
        list.append(parent, tunnel_view_list)

    def add_port_to_port_view(self, port, list):
        """Add port to port view."""
        port_view_list = [port]
        port_view_list.extend(port.get_attributes())
        list.append(port_view_list)

    def confirm_quit(self):
        """Ask for confirmation if there are active tunnels."""
        if self.controller.has_active_tunnel():
            dialog = Gtk.MessageDialog(type=Gtk.MessageType.QUESTION,
                                       message_format="Tunnel(s) Active. Really quit?",
                                       buttons=Gtk.ButtonsType.OK_CANCEL)
            response = dialog.run()
            dialog.destroy()
            if response == Gtk.ResponseType.CANCEL:
                return False
        return True

    def on_quit(self, widget):
        """Handle explicit quit from menu"""
        if self.confirm_quit():
            if self.mainWindow is not None:
                self.quit_on_destroy = True
                self.destroy_window()
            else:
                if (self.config.safe_get_boolean('GUI', 'autoSave') and
                        self.controller.isDirty):
                    self.on_save(None)
                self.controller.stop_tunnels()
                Gtk.main_quit()

    def on_main_window_delete_event(self, widget, event):
        """Handle main window close button click"""
        if (self.config.safe_get_boolean('GUI', 'showIcon') and
                self.config.safe_get_boolean('GUI', 'minToTray')):
            self.destroy_window()
        elif self.confirm_quit():
            self.quit_on_destroy = True
            self.destroy_window()
        else:
            return True

    def on_main_window_destroy(self, widget):
        """Handle object destroy for application window"""
        if not self.quit_on_destroy:
            return True
        if (self.config.safe_get_boolean('GUI', 'autoSave') and
                self.controller.isDirty):
            self.on_save(None)
        self.controller.stop_tunnels()
        Gtk.main_quit()

    def on_main_window_window_state_event(self, widget, event):
        """Hide window when minimized with visible system tray."""
        if ((event.changed_mask & Gdk.WindowState.ICONIFIED) and
                (event.new_window_state & Gdk.WindowState.ICONIFIED) and
                self.config.safe_get_boolean('GUI', 'showIcon') and
                self.config.safe_get_boolean('GUI', 'minToTray') and
                self.statusIcon.is_enabled()):
            self.destroy_window()

    def on_main_window_focus_change_event(self, widget, event):
        """Track visibility state for status icon clicks."""
        if event.in_:
            self.in_focus = True
        else:
            self.in_focus = False
        return False

    def on_new(self, widget):
        """Handle File|New"""
        if self.controller.has_active_tunnel():
            self.errorReporter.show_error("Tunnel(s) Active. Stop before clearing config")
            return
        self.controller.clear()
        self.tunnelList.clear()
        self.keyList.clear()
        self.set_title_from_filename(self.controller.filename)
        if (self.statusIcon.is_enabled()):
            self.statusIcon.reload_menu()

    def on_open(self, widget):
        """Handle File|Open"""
        if self.controller.has_active_tunnel():
            self.errorReporter.show_error("Tunnel(s) Active. Stop before clearing config")
            return
        start_dir = self.controller.configpath
        if self.controller.filename:
            start_dir = os.path.dirname(os.path.realpath(self.controller.filename))
        if not os.path.isdir(start_dir):
            start_dir = None
        newFile = self.select_file(Gtk.FileChooserAction.OPEN,
                                   [self.filter],
                                   base_directory=start_dir,
                                   file_name=self.controller.filename,
                                   file_extension=".xml")
        if newFile:
            self.controller.load_file(newFile)
            self.load_tunnel_view()
            self.set_title_from_filename(self.controller.filename)
            if self.statusIcon.is_enabled():
                self.statusIcon.reload_menu()

    def on_save(self, widget):
        """Handle File|Save"""
        if self.controller.filename:
            self.controller.save_file()
            self.set_title_from_filename(self.controller.filename)
        else:
            self.on_save_as(widget)

    def on_save_as(self, widget):
        """Handle File|Save As"""
        start_dir = self.controller.configpath
        if self.controller.filename:
            start_dir = os.path.dirname(os.path.realpath(self.controller.filename))
        if not os.path.isdir(start_dir):
            start_dir = None
        newFile = self.select_file(Gtk.FileChooserAction.SAVE,
                                   [self.filter],
                                   base_directory=start_dir,
                                   file_name=self.controller.filename,
                                   file_extension=".xml")
        if newFile:
            self.controller.save_file(newFile)
            self.set_title_from_filename(newFile)

    def on_add_tunnel(self, widget):
        """Handle Tunnel|Add"""
        self.run_tunnel_dialog(widget)

    def on_edit_tunnel(self, widget):
        """Handle Tunnel|Edit"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected:
            tunnel = model.get_value(selected, 0)
            if tunnel.isActive:
                self.errorReporter.show_error("Tunnel %s is active. Stop to edit." % tunnel.name)
                return
            self.run_tunnel_dialog(widget, selected)

    def run_tunnel_dialog(self, widget, oldTunnel=None):
        """Show the tunnel dialog: eventually needs to fill in port view"""
        self.tunnelTree = Gtk.Builder()
        self.tunnelTree.add_objects_from_file(self.uiFile, ["tunnelDialog"])
        self.tunnelTree.connect_signals(self)
        tunnelDialog = self.tunnelTree.get_object("tunnelDialog")
        self.portView = self.tunnelTree.get_object("portView")
        self.initialize_port_view(self.portView)

        name_entry = self.tunnelTree.get_object("nameEntry1")
        user_id_entry = self.tunnelTree.get_object("userIDEntry")
        tunnel_host_entry = self.tunnelTree.get_object("tunnelHostEntry")
        tunnel_port_entry = self.tunnelTree.get_object("tunnelPortEntry")
        key_entry = self.tunnelTree.get_object("keyEntry")

        selected_key = None
        old_tunnel_object = None
        if oldTunnel:
            old_tunnel_object = self.tunnelList.get_value(oldTunnel, 0)
            name_entry.set_text(old_tunnel_object.name)
            user_id_entry.set_text(old_tunnel_object.userid)
            tunnel_host_entry.set_text(old_tunnel_object.tunnelHost)
            tunnel_port_entry.set_text(old_tunnel_object.tunnelPort)
            selected_key = old_tunnel_object.requiredKey
            for port in old_tunnel_object.portList:
                self.add_port_to_port_view(port, self.portList)

        current_index = 0
        selected_index = 0
        key_list = Gtk.ListStore(GObject.TYPE_STRING)
        key_list.append([None])
        for key in self.controller.keyList:
            current_index = current_index + 1
            displayKey = os.path.basename(key.id)
            if (key.id != key.filename):
                displayKey = os.path.basename(key.filename) + " (" + displayKey + ")"
            key_list.append([displayKey])
            if selected_key and selected_key in (key.id, key.filename):
                selected_index = current_index
        if selected_key and selected_index == 0:
            key_list.append([selected_key])
            selected_index = current_index + 1
        key_entry.set_model(key_list)
        key_entry.set_active(selected_index)
        cell = Gtk.CellRendererText()
        key_entry.pack_start(cell, True)
        key_entry.add_attribute(cell, "text", 0)

        result = tunnelDialog.run()

        new_tunnel = None
        if result == Gtk.ResponseType.OK:
            selected_index = key_entry.get_active()
            if selected_index > 0:
                if selected_index <= len(self.controller.keyList):
                    selected_key = self.controller.keyList[(selected_index - 1)].filename
                # else, leave selectedKey alone. It must not have changed.
            else:
                selected_key = None

            new_tunnel = TunnelInfo(name_entry.get_text(),
                                    user_id_entry.get_text(),
                                    tunnel_host_entry.get_text(),
                                    tunnel_port_entry.get_text(),
                                    selected_key,
                                    self.errorReporter,
                                    self.controller)
            port = self.portList.get_iter_first()
            while port:
                new_tunnel.add_port(self.portList.get_value(port, 0))
                port = self.portList.iter_next(port)
            if old_tunnel_object:
                self.controller.remove_tunnel(old_tunnel_object)
            self.add_tunnel_to_tunnel_view(new_tunnel, self.tunnelList, oldTunnel)
            self.controller.insert_tunnel(new_tunnel)
            self.controller.isDirty = True
            if self.statusIcon.is_enabled():
                self.statusIcon.reload_menu()

        tunnelDialog.destroy()

    def on_tunnel_dialog_response(self, widget, response):
        """Validate the tunnel dialog."""
        if response != Gtk.ResponseType.OK:
            return False

        name_entry = self.tunnelTree.get_object("nameEntry1")
        tunnel_host_entry = self.tunnelTree.get_object("tunnelHostEntry")
        tunnel_port_entry = self.tunnelTree.get_object("tunnelPortEntry")

        invalid = False
        if not name_entry.get_text():
            self.errorReporter.show_error("The 'Name' field is required")
            invalid = True
        elif not tunnel_host_entry.get_text():
            self.errorReporter.show_error("The 'Tunnel Host' field is required")
            invalid = True
        elif not tunnel_port_entry.get_text():
            self.errorReporter.show_error("The 'Tunnel Port' field is required")
            invalid = True
        elif not self.portList.get_iter_first():
            self.errorReporter.show_error("Specify a port to tunnel.")
            invalid = True
        else:
            try:
                tunnel_port = int(tunnel_port_entry.get_text())
            except ValueError:
                self.errorReporter.show_error("Invalid 'Tunnel Port': %s" %
                                              tunnel_port_entry.get_text())
                invalid = True
            else:
                if tunnel_port < 1 or tunnel_port > 65535:
                    self.errorReporter.show_error("'Tunnel Port' out of range: %d" % tunnel_port)
                    invalid = True

        if (invalid):
            widget.emit_stop_by_name("response")
        return invalid

    def on_cursor_changed(self, widget):
        """Called when the selection changes in the tunnel view"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected and model.iter_depth(selected) != 0:
            parent = model.iter_parent(selected)
            selection.select_iter(parent)

    def on_row_activated(self, widget, path, column):
        """Called when a row is double-clicked in the tunnel view"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected and model.iter_depth(selected) == 0:
            tunnel = model.get_value(selected, 0)
            if tunnel.isActive:
                if tunnel.stop():
                    self.controller.isDirty = True
                    self.add_tunnel_to_tunnel_view(tunnel, model, selected)
            else:
                if tunnel.start(self.schedule_tunnel_cleanup):
                    self.controller.isDirty = True
                    self.add_tunnel_to_tunnel_view(tunnel, model, selected)
            if self.statusIcon.is_enabled():
                self.statusIcon.reload_menu()

    def on_tunnel_view_button_press_event(self, widget, event):
        """Display context sensitive popup menu."""
        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = event.time
            pthinfo = self.tunnelView.get_path_at_pos(x, y)
            if pthinfo != None:
                path, col, cellx, celly = pthinfo
                self.tunnelView.grab_focus()
                self.tunnelView.set_cursor(path, col, 0)
                self.show_tunnel_popup(event.button, time)
            return True

    def show_tunnel_popup(self, button, time):
        """Show a context-sensitive popup menu for a tunnel."""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected and model.iter_depth(selected) == 0:
            tunnel = model.get_value(selected, 0)
            menu = Gtk.Menu()
            if tunnel.isActive:
                self.add_to_menu(
                    menu, "Stop Tunnel", self.stop_tunnel, tunnel)
            else:
                self.add_to_menu(
                    menu, "Start Tunnel", self.start_tunnel, tunnel)
                self.add_to_menu(
                    menu, "Edit Tunnel", self.on_edit_tunnel)
                self.add_to_menu(
                    menu, "Remove Tunnel", self.on_remove_tunnel)
            menu.popup(None, None, None, None, button, time)

    def add_to_menu(self, menu, label, handler, data=None):
        """Add item to tunnel context menu."""
        item = Gtk.MenuItem(label)
        if handler:
            if data:
                item.connect('activate', handler, data)
            else:
                item.connect('activate', handler)
        menu.append(item)
        item.show()
        return item

    def start_tunnel(self, widget, tunnel):
        """Start tunnel and update GUI."""
        if not tunnel.isActive:
            if tunnel.start(self.schedule_tunnel_cleanup):
                self.controller.isDirty = True
                if self.mainWindow is not None:
                    self.update_tunnel(tunnel)
                if self.statusIcon.is_enabled():
                    self.statusIcon.reload_menu()

    def stop_tunnel(self, widget, tunnel):
        """Stop tunnel and update GUI."""
        if tunnel.isActive:
            if tunnel.stop():
                self.controller.isDirty = True
                if self.mainWindow is not None:
                    self.update_tunnel(tunnel)
                if self.statusIcon.is_enabled():
                    self.statusIcon.reload_menu()

    def on_remove_tunnel(self, widget):
        """Handle Tunnel|Remove"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected:
            tunnel = model.get_value(selected, 0)
            if tunnel.isActive:
                self.errorReporter.show_error("Tunnel %s is active. Stop to remove." % tunnel.name)
                return
            model.remove(selected)
            self.controller.remove_tunnel(tunnel)
            self.controller.isDirty = True
            if self.statusIcon.is_enabled():
                self.statusIcon.reload_menu()

    def on_start_tunnel(self, widget):
        """Handle Tunnel|Start"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected:
            tunnel_object = model.get_value(selected, 0)
            if tunnel_object.start(self.schedule_tunnel_cleanup):
                self.controller.isDirty = True
                self.add_tunnel_to_tunnel_view(tunnel_object, model, selected)
                if self.statusIcon.is_enabled():
                    self.statusIcon.reload_menu()

    def on_stop_tunnel(self, widget):
        """Handle Tunnel|Stop"""
        selection = self.tunnelView.get_selection()
        model, selected = selection.get_selected()
        if selected:
            tunnel_object = model.get_value(selected, 0)
            if tunnel_object.stop():
                self.controller.isDirty = True
                self.add_tunnel_to_tunnel_view(tunnel_object, model, selected)
                if self.statusIcon.is_enabled():
                    self.statusIcon.reload_menu()

    def on_about(self, widget):
        """Handle Help|About"""
        about_dialog = Gtk.AboutDialog()
        about_dialog.set_name("tunnelmgr")
        about_dialog.set_authors([__author__, "Andreas Kremser"])
        about_dialog.set_version(__version__)
        about_dialog.set_copyright(__copyright__)
        about_dialog.set_license(__license__)
        about_dialog.run()
        about_dialog.destroy()

    def on_add_port(self, widget):
        """Handle Tunnel Info|Add Port button click"""
        self.portTree = Gtk.Builder()
        self.portTree.add_objects_from_file(self.uiFile, ["portDialog"])
        self.portTree.connect_signals(self)
        portDialog = self.portTree.get_object("portDialog")
        typeEntry = self.portTree.get_object("typeEntry")
        typeList = Gtk.ListStore(GObject.TYPE_STRING)
        typeList.append(["Local"])
        typeList.append(["Remote"])
        typeList.append(["Dynamic"])
        typeEntry.set_model(typeList)
        typeEntry.set_active(0)
        cell = Gtk.CellRendererText()
        typeEntry.pack_start(cell, True)
        typeEntry.add_attribute(cell, "text", 0)
        result = portDialog.run()
        if (result == Gtk.ResponseType.OK):
            # User clicked 'OK' in port dialog: apply preferences"
            newPort = None
            nameEntry = self.portTree.get_object("nameEntry2")
            bindIPEntry = self.portTree.get_object("bindIPEntry")
            localPortEntry = self.portTree.get_object("localPortEntry")
            if (typeEntry.get_active() == PortInfo.LOCAL_TYPE or
                    typeEntry.get_active() == PortInfo.REMOTE_TYPE):
                remoteHostEntry = self.portTree.get_object("remoteHostEntry")
                remotePortEntry = self.portTree.get_object("remotePortEntry")
                newPort = PortInfo.ForwardPort(typeEntry.get_active(),
                                               nameEntry.get_text(),
                                               bindIPEntry.get_text(),
                                               localPortEntry.get_text(),
                                               remoteHostEntry.get_text(),
                                               remotePortEntry.get_text())
            else:
                newPort = PortInfo.DynamicPort(nameEntry.get_text(),
                                               bindIPEntry.get_text(),
                                               localPortEntry.get_text())
            self.add_port_to_port_view(newPort, self.portList)
        portDialog.destroy()

    def on_remove_port(self, widget):
        """Handle Tunnel Info|Remove Port button click"""
        selection = self.portView.get_selection()
        model, selection_iter = selection.get_selected()
        if (selection_iter):
            model.remove(selection_iter)

    def on_type_entry_changed(self, widget):
        """Handle change to typeEntry on portDialog"""
        type_entry = self.portTree.get_object("typeEntry")
        enabled = True
        if type_entry.get_active() == PortInfo.DYNAMIC_TYPE:
            enabled = False
        remote_host_label = self.portTree.get_object("remoteHostLabel")
        remote_host_label.set_sensitive(enabled)
        remote_host_entry = self.portTree.get_object("remoteHostEntry")
        remote_host_entry.set_sensitive(enabled)
        remote_port_label = self.portTree.get_object("remotePortLabel")
        remote_port_label.set_sensitive(enabled)
        remote_port_entry = self.portTree.get_object("remotePortEntry")
        remote_port_entry.set_sensitive(enabled)

    def on_port_dialog_response(self, widget, response):
        """Validate the port dialog."""
        if response != Gtk.ResponseType.OK:
            return False

        type_entry = self.portTree.get_object("typeEntry")
        local_port_entry = self.portTree.get_object("localPortEntry")
        remote_host_entry = self.portTree.get_object("remoteHostEntry")
        remote_port_entry = self.portTree.get_object("remotePortEntry")

        invalid = False
        if not local_port_entry.get_text():
            self.errorReporter.show_error("The 'Local Port' field is required")
            invalid = True
        else:
            port = None
            try:
                port = int(local_port_entry.get_text())
            except ValueError:
                self.errorReporter.show_error("Invalid 'Local Port': %s" %
                                              local_port_entry.get_text())
                invalid = True
            else:
                if port < 1 or port > 65535:
                    self.errorReporter.show_error("'Local Port' out of range: %d" % port)
                    invalid = True

        if not invalid and type_entry.get_active() != PortInfo.DYNAMIC_TYPE:
            if not remote_host_entry.get_text():
                self.errorReporter.show_error("The 'Remote Host' field is required")
                invalid = True
            elif not remote_port_entry.get_text():
                self.errorReporter.show_error("The 'Remote Port' field is required")
                invalid = True
            else:
                port = None
                try:
                    port = int(remote_port_entry.get_text())
                except ValueError:
                    self.errorReporter.show_error("Invalid 'Remote Port': %s" %
                                                  remote_port_entry.get_text())
                    invalid = True
                else:
                    if port < 1 or port > 65535:
                        self.errorReporter.show_error("'Remote Port' out of range: %d" % port)
                        invalid = True

        if (invalid):
            widget.emit_stop_by_name("response")
        return invalid

    def on_manage_keys(self, widget):
        """Run the SSH keys dialog"""
        self.keyTree = Gtk.Builder()
        self.keyTree.add_objects_from_file(self.uiFile, ["keyDialog"])
        self.keyTree.connect_signals(self)
        keyDialog = self.keyTree.get_object("keyDialog")
        self.keyView = self.keyTree.get_object("keyView")
        self.initialize_key_view(self.keyView)
        self.load_key_view()
        keyDialog.run()
        keyDialog.destroy()

    def on_add_key(self, widget):
        """Add an ssh key to the list of registered keys"""
        title = "Select Private Key"
        directory = os.environ.get("HOME") + "/.ssh"
        filename = self.select_file(Gtk.FileChooserAction.OPEN, [],
                                    title=title,
                                    base_directory=directory)
        if filename != "":
            new_key = KeyInfo(filename)
            self.controller.insert_key(new_key)
            self.load_key_view()
            self.controller.isDirty = True

    def on_remove_key(self, widget):
        """Remove an ssh key from the list of registered keys"""
        selection = self.keyView.get_selection()
        model, selection_iter = selection.get_selected()
        if selection_iter:
            key = self.keyList.get_value(selection_iter, 0)
            self.controller.remove_key(key)
            self.load_key_view()
            self.controller.isDirty = True

    def on_load_keys(self, widget):
        """Load all registered keys into the ssh-agent"""
        proc = self.controller.start_load_keys()
        if proc:
            GObject.timeout_add(100, self.cleanup_load_keys, proc)

    def on_unload_keys(self, widget):
        """Unload all registered keys from the ssh-agent"""
        self.controller.unload_keys()

    def on_view_agent(self, widget):
        """View details of currently running ssh agent"""
        agent_tree = Gtk.Builder()
        agent_tree.add_objects_from_file(self.uiFile, ["agentDialog"])
        agent_dialog = agent_tree.get_object("agentDialog")
        agent_view = agent_tree.get_object("agentView")
        agent_list = self.initialize_key_view(agent_view)
        self.load_agent_dialog(agent_tree, agent_view.get_model())
        agent_dialog.run()
        agent_dialog.destroy()

    def load_agent_dialog(self, agentTree, agentList):
        """Fill the data fields in the agent dialog"""
        pid = os.environ.get("SSH_AGENT_PID")
        if pid is not None:
            pidEntry = agentTree.get_object("pidEntry")
            pidEntry.set_text(pid)
        sock = os.environ.get("SSH_AUTH_SOCK")
        if sock is not None:
            sockEntry = agentTree.get_object("sockEntry")
            sockEntry.set_text(sock)
        pipe = subprocess.Popen(["/usr/bin/ssh-add", "-l"],
                                stdout=subprocess.PIPE)
        stdout = pipe.communicate()[0]
        if pipe.returncode == 0:
            for l in stdout.splitlines():
                line=str(l,'utf-8')
                fields = line.split()
                if len(fields) < 4:
                    continue
                key = self.controller.lookup_key(fields[2])
                filename = os.path.basename(key.filename) if key else None
                agentList.append([None, filename,
                                  os.path.basename(fields[2]),
                                  fields[3], fields[0], fields[1]])

    def on_preferences(self, widget):
        """Run the preferences dialog."""
        self.preferencesTree = Gtk.Builder()
        self.preferencesTree.add_objects_from_file(self.uiFile, ["preferencesDialog"])
        self.preferencesTree.connect_signals(self)
        preferencesDialog = self.preferencesTree.get_object("preferencesDialog")
        preferencesDialog.set_focus(None)

        defaultFileEntry = self.preferencesTree.get_object('defaultFileEntry')
        autoSaveEntry = self.preferencesTree.get_object('autoSaveEntry')
        restoreStateEntry = self.preferencesTree.get_object('restoreStateEntry')
        loadKeysEntry = self.preferencesTree.get_object('loadKeysEntry')
        showIconEntry = self.preferencesTree.get_object('showIconEntry')
        minToTrayEntry = self.preferencesTree.get_object('minToTrayEntry')
        minToTrayLabel = self.preferencesTree.get_object('minToTrayLabel')
        launchInTrayEntry = self.preferencesTree.get_object('launchInTrayEntry')
        launchInTrayLabel = self.preferencesTree.get_object('launchInTrayLabel')

        # Load current config values.
        defaultFileEntry.set_text(self.config.safe_get('BASE', 'defaultFile'))
        autoSaveEntry.set_active(self.config.safe_get_boolean('GUI', 'autoSave'))
        restoreStateEntry.set_active(self.config.safe_get_boolean('GUI', 'restoreState'))
        loadKeysEntry.set_active(self.config.safe_get_boolean('GUI', 'loadKeys'))
        minToTrayEntry.set_active(self.config.safe_get_boolean('GUI', 'minToTray'))
        launchInTrayEntry.set_active(self.config.safe_get_boolean('GUI', 'launchInTray'))
        showIconEntry.set_active(self.config.safe_get_boolean('GUI', 'showIcon'))

        minToTrayLabel.set_sensitive(showIconEntry.get_active())
        minToTrayEntry.set_sensitive(showIconEntry.get_active())
        launchInTrayLabel.set_sensitive(showIconEntry.get_active() and
                                        minToTrayEntry.get_active())
        launchInTrayEntry.set_sensitive(showIconEntry.get_active() and
                                        minToTrayEntry.get_active())

        # Run dialog and update config values.
        result = preferencesDialog.run()
        if result == Gtk.ResponseType.OK:
            needSave = False
            if (defaultFileEntry.get_text() !=
                    self.config.safe_get('BASE', 'defaultFile')):
                self.config.safe_set('BASE', 'defaultFile',
                                     defaultFileEntry.get_text())
                needSave = True
            if (autoSaveEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'autoSave')):
                self.config.safe_set('GUI', 'autoSave',
                                     "%s" % autoSaveEntry.get_active())
                needSave = True
            if (restoreStateEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'restoreState')):
                self.config.safe_set('GUI', 'restoreState',
                                     "%s" % restoreStateEntry.get_active())
                needSave = True
            if (loadKeysEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'loadKeys')):
                self.config.safe_set('GUI', 'loadKeys',
                                     "%s" % loadKeysEntry.get_active())
                needSave = True
            if (showIconEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'showIcon')):
                self.config.safe_set('GUI', 'showIcon',
                                     "%s" % showIconEntry.get_active())
                if (showIconEntry.get_active()):
                    self.statusIcon.enable()
                else:
                    self.statusIcon.disable()
                needSave = True
            if (minToTrayEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'minToTray')):
                self.config.safe_set('GUI', 'minToTray',
                                     "%s" % minToTrayEntry.get_active())
                needSave = True
            if (launchInTrayEntry.get_active() !=
                    self.config.safe_get_boolean('GUI', 'launchInTray')):
                self.config.safe_set('GUI', 'launchInTray',
                                     "%s" % launchInTrayEntry.get_active())
                needSave = True
            if needSave:
                self.config.write()

        preferencesDialog.destroy()

    def on_icon_entry_toggled(self, widget):
        """An icon-related checkbox has been toggled."""
        show_icon_entry = self.preferencesTree.get_object('showIconEntry')
        min_to_tray_label = self.preferencesTree.get_object('minToTrayLabel')
        min_to_tray_entry = self.preferencesTree.get_object('minToTrayEntry')
        min_to_tray_label.set_sensitive(show_icon_entry.get_active())
        min_to_tray_entry.set_sensitive(show_icon_entry.get_active())
        launch_in_tray_entry = self.preferencesTree.get_object('launchInTrayEntry')
        launch_in_tray_label = self.preferencesTree.get_object('launchInTrayLabel')
        launch_in_tray_label.set_sensitive(show_icon_entry.get_active() and
                                           min_to_tray_entry.get_active())
        launch_in_tray_entry.set_sensitive(show_icon_entry.get_active() and
                                           min_to_tray_entry.get_active())

    def on_default_file_selector_clicked(self, widget):
        """Run file selector."""
        default_file_entry = self.preferencesTree.get_object('defaultFileEntry')
        start_file = default_file_entry.get_text()
        start_dir = os.path.dirname(start_file)
        if not os.path.isdir(start_dir):
            start_dir = None
        new_file = self.select_file(Gtk.FileChooserAction.SAVE,
                                    [self.filter],
                                    base_directory=start_dir,
                                    file_name=start_file,
                                    file_extension=".xml",
                                    title="Select Default Configuration")
        if new_file:
            default_file_entry.set_text(new_file)

    def cleanup_load_keys(self, process):
        """Cleanup a running 'loadKeys' operation."""
        return self.controller.check_load_keys_complete(process)

    def cleanup_tunnel(self, tunnel):
        """Cleanup a running 'TunnelInfo.start' operation."""

        process = tunnel.process
        if process is None:
            return False

        if process.poll() is not None:
            errMsg = "Unexpected termination of tunnel: %s" % tunnel.name
            if process.returncode != 0:
                errMsg = errMsg + "\n\n" + str(process.stderr.read(), 'utf-8') + str(process.stdout.read(), 'utf-8')
            tunnel.errorReporter.show_error(errMsg)
            tunnel.process = None
            tunnel.stop()
            self.controller.isDirty = True
            self.update_tunnel(tunnel)
            return False

        # still running
        return True

    def schedule_tunnel_cleanup(self, tunnel):
        """Schedule tunnel cleanup after tunnel start."""
        GObject.timeout_add(100, self.cleanup_tunnel, tunnel)

    def wait_for_auto_load(self):
        """Autoload tunnels if ssh-add is done."""
        if self.controller.isSSHAddRunning:
            return True

        if not self.controller.default_route_exists():
            return True

        self.controller.start_tunnels(self.schedule_tunnel_cleanup, True)
        if self.mainWindow is not None:
            self.load_tunnel_view()
        if self.statusIcon.is_enabled():
            self.statusIcon.reload_menu()
        return False

    def initial_iconify(self):
        """Handle initial iconification."""
        self.mainWindow.iconify()
        return False

    def select_file(self, action, filters, title=None,
                    file_extension=None, file_name=None, base_directory=None):
        """This function is used to select a file to open"""

        if action == Gtk.FileChooserAction.OPEN:
            buttons = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                       Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
            if title is None:
                title = "Open File"
        else:
            buttons = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                       Gtk.STOCK_SAVE, Gtk.ResponseType.OK)
            if title is None:
                title = "Save File"

        dialog = Gtk.FileChooserDialog(title=title,
                                       action=action,
                                       buttons=buttons)
        if action == Gtk.FileChooserAction.SAVE and file_name:
            dialog.set_current_name(file_name)
            base_directory = os.path.dirname(file_name)
        if base_directory:
            dialog.set_current_folder(base_directory)

        for filter in filters:
            dialog.add_filter(filter)
        filter = Gtk.FileFilter()
        filter.set_name("All files")
        filter.add_pattern("*")
        dialog.add_filter(filter)

        result = ""
        if dialog.run() == Gtk.ResponseType.OK:
            result = dialog.get_filename()
            if action == Gtk.FileChooserAction.SAVE:
                result, extension = os.path.splitext(result)
                if (not extension or extension == "") and file_extension:
                    result = result + file_extension
                elif extension and extension != "":
                    result = result + extension
        dialog.destroy()

        return result
