# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnelmanager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


LOCAL_TYPE = 0
REMOTE_TYPE = 1
DYNAMIC_TYPE = 2


class PortInfo(object):
    """This object represents port forwarding information"""

    def __get_type(self):
        return self.__m_type

    type = property(__get_type)

    def __init__(self, type):
        """The only way to set the type is via initialization"""
        self.__m_type = type

    def is_match(self, port):
        """Is specified port a match?"""
        if port.type != self.type:
            return False
        return True

    def get_type_string(self):
        """Get the type to display as a string"""
        return "unknown"

    def get_attributes(self):
        """Get the port's attributes as a list."""
        return_list = [self.type]
        return return_list

    def command_string(self):
        """Returns the port in the form for a command line arg"""
        return ""

    def write(self, document, tunnel_element):
        """Write the port to the XML document"""
        return


class ForwardPort(PortInfo):
    """This is a forwarding type of port"""

    def __init__(self, portType, name, bindIP, localPort, remoteHost, remotePort):
        """The constructor"""
        self.bindIP = bindIP
        self.name = name
        self.localPort = localPort
        self.remoteHost = remoteHost
        self.remotePort = remotePort
        PortInfo.__init__(self, portType)

    def __str__(self):
        return "<port type='%s' name='%s' bind_ip='%s' local_port='%s' remote_host='%s' remote_port='%s'>" % (
            self.get_type_string(), self.name, self.bindIP, self.localPort, self.remoteHost, self.remotePort)

    def is_match(self, port):
        """Is specified port a match?"""
        if (not PortInfo.is_match(self, port) or
                port.name != self.name or
                port.bindIP != self.bindIP or
                port.localPort != self.localPort or
                port.remoteHost != self.remoteHost or
                port.remotePort != self.remotePort):
            return False
        return True

    def command_string(self):
        """Return the command line string"""
        flag = ""
        if self.type == LOCAL_TYPE:
            flag = "-L"
        if self.type == REMOTE_TYPE:
            flag = "-R"
        if self.bindIP:
            return "%s%s:%s:%s:%s" % (flag, self.bindIP, self.localPort, self.remoteHost, self.remotePort)
        else:
            return "%slocalhost:%s:%s:%s" % (flag, self.localPort, self.remoteHost, self.remotePort)

    def get_type_string(self):
        """Get the type as a string"""
        if self.type == LOCAL_TYPE:
            return "local"
        if self.type == REMOTE_TYPE:
            return "remote"

    def get_attributes(self):
        """Get port attributes as a list; with type as string."""
        return_list = [self.get_type_string(),
                       self.name,
                       self.bindIP,
                       self.localPort,
                       self.remoteHost,
                       self.remotePort]
        return return_list

    def write(self, document, tunnel_element):
        """Write the port to XML"""
        port_element = document.createElement("Port")
        port_element.setAttribute("type", self.get_type_string())
        port_element.setAttribute("name", self.name)
        port_element.setAttribute("bind_ip", self.bindIP)
        port_element.setAttribute("local_port", self.localPort)
        port_element.setAttribute("remote_host", self.remoteHost)
        port_element.setAttribute("remote_port", self.remotePort)
        tunnel_element.appendChild(port_element)


class DynamicPort(PortInfo):
    """This is a dynamic type of port"""

    def __init__(self, name, bindIP, localPort):
        """The constructor"""
        self.name = name
        self.bindIP = bindIP
        self.localPort = localPort
        PortInfo.__init__(self, DYNAMIC_TYPE)

    def __str__(self):
        return "<port type='%s' name='%s' bind_ip='%s' local_port='%s'>" % (
            self.get_type_string(), self.name, self.bindIP, self.localPort)

    def is_match(self, port):
        """Is specified port a match?"""
        if (not PortInfo.is_match(self, port) or
                port.name != self.name or
                port.bindIP != self.bindIP or
                port.localPort != self.localPort):
            return False
        return True

    def command_string(self):
        """Return the command line string"""
        if self.bindIP:
            return "-D%s:%s" % (self.bindIP, self.localPort)
        else:
            return "-Dlocalhost:%s" % self.localPort

    def get_type_string(self):
        """Get the type as a string"""
        return "dynamic"

    def get_attributes(self):
        """Get port attributes as a list; with type as string."""
        return_list = [self.get_type_string(),
                       self.name,
                       self.bindIP,
                       self.localPort,
                       None,  # All port types must have the same attr count.
                       None]  # All port types must have the same attr count.
        return return_list

    def write(self, document, tunnel_element):
        """Write the port to XML"""
        port_element = document.createElement("Port")
        port_element.setAttribute("type", "dynamic")
        port_element.setAttribute("name", self.name)
        port_element.setAttribute("bind_ip", self.bindIP)
        port_element.setAttribute("local_port", self.localPort)
        tunnel_element.appendChild(port_element)
