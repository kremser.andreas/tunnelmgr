# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnelmanager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


import os
import signal
import sys
import subprocess
from functools import total_ordering


@total_ordering
class TunnelInfo(object):
    """This object represents an ssh tunnel with one or more ports"""

    def __is_active(self):
        return self.__m_active

    isActive = property(__is_active)

    def __get_port_list(self):
        return self.__m_portList

    portList = property(__get_port_list)

    def __init__(self, name, userid, tunnelHost, tunnelPort, requiredKey,
                 errorReporter, controller, needsActivate=False):
        """The constructor"""
        self.__m_active = False
        self.name = name
        self.userid = userid
        self.tunnelHost = tunnelHost
        self.tunnelPort = tunnelPort
        self.requiredKey = requiredKey
        self.__m_needsActivate = needsActivate

        self.__m_portList = []

        self.errorReporter = errorReporter
        self.controller = controller

        self.process = None
        self.callbackSourceID = None

    def __lt__(self, other):
        return self.name < other.name

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return "<tunnel active='%s' name='%s' userid='%s' tunnelHost='%s' tunnelPort='%s' requiredKey='%s' " \
               "numPorts='%d'>" % (
                   (self.isActive or self.__m_needsActivate), self.name, self.userid, self.tunnelHost, self.tunnelPort,
                   self.requiredKey, len(self.__m_portList))

    def needs_activate(self):
        return self.__m_needsActivate

    def cancel_activate(self):
        self.__m_needsActivate = False

    def clear_ports(self):
        """Remove all ports"""
        self.__m_portList.clear()

    def add_port(self, port):
        """Add a port"""
        self.__m_portList.append(port)

    def remove_port(self, port):
        """Remove a port"""
        try:
            self.__m_portList.remove(port)
        except ValueError as e:
            self.errorReporter.show_error("Error removing port %(port)s = %(err)s" % {'port': port, 'err': e})
            pass

    def lookup_port(self, find_port):
        """Lookup a matching port; all fields must matching."""
        for port in self.portList:
            if port.is_match(find_port):
                return port
        return None

    def get_attributes(self):
        """Get tunnel attributes as a list."""
        return_list = [self.isActive, self.name, self.userid, self.tunnelHost, self.tunnelPort]
        id_to_display = self.requiredKey
        if id_to_display:
            key = self.controller.lookup_key(id_to_display)
            if key:
                id_to_display = key.id
            return_list.append(os.path.basename(id_to_display))
        else:
            return_list.append(None)
        return return_list

    def write(self, document):
        """Write tunnel definition to XML document"""
        tunnel_element = document.createElement("Tunnel")
        tunnel_element.setAttribute("name", self.name)
        tunnel_element.setAttribute("state", "active" if (self.isActive or self.__m_needsActivate) else "inactive")
        tunnel_element.setAttribute("userid", self.userid)
        tunnel_element.setAttribute("tunnel_host", self.tunnelHost)
        tunnel_element.setAttribute("tunnel_port", self.tunnelPort)
        if self.requiredKey:
            tunnel_element.setAttribute("required_key", self.requiredKey)
        for port in self.__m_portList:
            port.write(document, tunnel_element)
        document.documentElement.appendChild(tunnel_element)

    def check_required_key(self):
        """Check whether the required key is known to the ssh agent"""
        if not self.requiredKey:
            return True

        id_to_check = self.requiredKey
        key = self.controller.lookup_key(self.requiredKey)
        if key:
            id_to_check = key.id

        pipe = subprocess.Popen(["/usr/bin/ssh-add", "-l"],
                                stdout=subprocess.PIPE)
        stdout = pipe.communicate()[0]
        for l in stdout.splitlines():
            line=str(l,'utf-8')
            keyid = line.split()[2]
            if id_to_check == keyid:
                return True

        return False

    def start(self, schedule_tunnel_cleanup):
        """Start the tunnel"""
        if self.__m_active:
            self.errorReporter.show_error("Tunnel already active: %s" % self.name)
            return False

        key = None
        if not self.check_required_key():
            key = self.controller.lookup_key(self.requiredKey)
            if not key:
                self.errorReporter.show_error(
                    "Required key unknown to agent: %s" % self.requiredKey)
                return False

        bin_path = os.path.dirname(os.path.realpath(sys.argv[0]))
        cmdline = [bin_path + "/tunnel_runner", "GUI"]
        cmdline.extend(self.controller.sshbaseargs)
        if key:
            cmdline.append("-i%s" % key.filename)
        if self.userid:
            cmdline.append("-l%s" % self.userid)
        for port in self.__m_portList:
            cmdline.append(port.command_string())
        cmdline.append("-p%s" % self.tunnelPort)
        cmdline.append(self.tunnelHost)

        self.process = subprocess.Popen(cmdline,
                                        stderr=subprocess.PIPE,
                                        stdout=subprocess.PIPE)
        self.__m_active = True
        schedule_tunnel_cleanup(self)
        return True

    def get_tunnel_fd(self):
        """Get the tunnel FD for an active tunnel"""
        if not self.__m_active or self.process is None:
            return -1
        return self.process.stdout.fileno()

    def stop(self):
        """Stop the tunnel"""
        if not self.__m_active:
            self.errorReporter.show_error("Tunnel already inactive: %s" % self.name)
            return False

        proc = self.process
        self.process = None
        self.__m_active = False
        if proc and not proc.returncode:
            try:
                os.kill(proc.pid, signal.SIGINT)
                os.waitpid(proc.pid, 0)
            except OSError as e:
                self.errorReporter.show_error(
                    "Failed closing tunnel: %(name)s (%(errno)d/%(errstr)s)" % {'name': self.name, 'errno': e.errno,
                                                                                'errstr': e.strerror})
                pass
        return True
