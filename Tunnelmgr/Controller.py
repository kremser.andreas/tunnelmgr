# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnelmanager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import os
import sys
import subprocess
from xml.dom import minidom

from Tunnelmgr import PortInfo
from Tunnelmgr.KeyInfo import KeyInfo
from Tunnelmgr.TunnelInfo import TunnelInfo


class Controller(object):
    """The configuration controller."""

    def __is_dirty(self):
        return self.__m_dirty

    def __set_is_dirty(self, dirty):
        self.__m_dirty = dirty

    isDirty = property(__is_dirty, __set_is_dirty)

    def __get_filename(self):
        return self.__m_filename

    def __set_filename(self, name):
        self.__m_filename = name

    filename = property(__get_filename, __set_filename)

    def __get_config_path(self):
        return self.__m_configpath

    configpath = property(__get_config_path)

    def __get_ip_cmd_path(self):
        return self.__m_ipcmdpath

    ipcmdpath = property(__get_ip_cmd_path)

    def __get_ssh_base_args(self):
        return self.__m_sshbaseargs

    sshbaseargs = property(__get_ssh_base_args)

    def __is_ssh_add_running(self):
        return self.__m_sshAddRunning

    isSSHAddRunning = property(__is_ssh_add_running)

    tunnelList = []

    keyList = []

    def __init__(self, reporter, config, tunnel_file=None):
        """The object constructor"""
        self.errorReporter = reporter

        self.__m_filename = None
        self.__m_dirty = False
        self.__m_sshAddRunning = False

        self.__m_keyList = []
        self.__m_tunnelList = []

        if 'XDG_CONFIG_HOME' in os.environ:
            config_home = os.environ['XDG_CONFIG_HOME']
        else:
            config_home = os.path.join(os.path.expanduser('~'), '.config')

        self.__m_configpath = config_home + "/tunnelmgr"
        if not os.path.isdir(self.configpath):
            try:
                os.mkdir(self.configpath)
            except OSError as e:
                print("Failed creating directory %s: %s" % (self.configpath, str(e)), file=sys.stderr)
                pass

        config.safe_set('DEFAULT', 'ipcmdpath', "/sbin/ip")
        self.__m_ipcmdpath = config.safe_get('BASE', 'ipcmdpath')

        config.safe_set('DEFAULT', 'sshbaseargs',
                        ["-N", "-t", "-x", "-o", "ExitOnForwardFailure=yes"])
        tmpstr = config.safe_get('BASE', 'sshbaseargs')
        tmplist = None
        if isinstance(tmpstr, list):
            tmplist = tmplist
        elif isinstance(tmpstr, str):
            try:
                tmplist = eval(tmpstr)
            except:
                print("Bad 'BASE' sshbaseargs config, using DEFAULT", file=sys.stderr)
        if not isinstance(tmplist, list):
            tmplist = config.safe_get('DEFAULT', 'sshbaseargs')
        self.__m_sshbaseargs = tmplist

        config.safe_set('DEFAULT', 'defaultFile',
                        self.configpath + "/default.xml")
        if not tunnel_file:
            tunnel_file = config.safe_get('BASE', 'defaultFile')

        if tunnel_file and os.path.exists(tunnel_file):
            self.load_file(tunnel_file)
        else:
            self.__m_filename = tunnel_file

    def load_file(self, fileName):
        """Load from config file"""
        parse_result = False
        oldKeyList = self.keyList
        del self.keyList[:]
        oldTunnelList = self.tunnelList
        del self.tunnelList[:]
        try:
            xml_document = minidom.parse(fileName)
            parse_result = ((self.read_keys(xml_document)) and
                            (self.read_tunnels(xml_document)))
        except OSError as e:
            self.errorReporter.show_error(
                "Error loading file: %(file)s: (%(errno)d) %(errstr)s" % {'file': e.filename, 'errno': e.errno,
                                                                          'errstr': e.strerror})
        except TypeError as e:
            self.errorReporter.show_error("Error parsing XML file: %(file)s: %(err)s" % {'file': fileName, 'err': e})
        except AttributeError as e:
            self.errorReporter.show_error("Error parsing XML file: %(file)s: %(err)s" % {'file': fileName, 'err': e})
        if not parse_result:
            self.keyList = oldKeyList
            self.tunnelList = oldTunnelList
        else:
            self.filename = fileName
            self.convert_required_keys()
        self.__m_dirty = False
        return parse_result

    def read_keys(self, document):
        """Read keys from XML document"""
        for node in document.documentElement.childNodes:
            if node.nodeName == "SSHKey":
                keyFile = node.getAttribute("filename")
                if keyFile:
                    newKey = KeyInfo(keyFile)
                    self.keyList.append(newKey)
                else:
                    self.errorReporter.show_error("Invalid XML: SSHKey without filename")
                    return False
        self.update_keys_from_agent()
        return True

    def read_tunnels(self, document):
        """Read Tunnels from XML document"""
        """I don't like this one, it isn't modular enough
           so I'll have to work on that"""
        for node in document.documentElement.childNodes:
            if node.nodeName == "Tunnel":
                name = node.getAttribute("name")
                userid = node.getAttribute("userid")
                tunnel_host = node.getAttribute("tunnel_host")
                tunnel_port = node.getAttribute("tunnel_port")
                required_key = node.getAttribute("required_key")
                state = node.getAttribute("state")
                needs_activate = True if state == "active" else False
                new_tunnel = TunnelInfo(name,
                                        userid,
                                        tunnel_host,
                                        tunnel_port,
                                        required_key,
                                        self.errorReporter,
                                        self,
                                        needs_activate)

                for pnode in node.childNodes:
                    if pnode.nodeName == "Port":
                        type = pnode.getAttribute("type")
                        name = pnode.getAttribute("name")
                        bind_ip = pnode.getAttribute("bind_ip")
                        local_port = pnode.getAttribute("local_port")
                        new_port = None
                        if type in ("local", "local fwd", "remote", "remote fwd"):
                            remote_host = pnode.getAttribute("remote_host")
                            remote_port = pnode.getAttribute("remote_port")
                            if (not local_port or
                                    not remote_host or
                                    not remote_port):
                                self.errorReporter.show_error(
                                    "Invalid XML: Port missing attribute(s)")
                                return False
                            if type in ("local", "local fwd"):
                                new_port = PortInfo.ForwardPort(
                                    PortInfo.LOCAL_TYPE,
                                    name,
                                    bind_ip,
                                    local_port,
                                    remote_host,
                                    remote_port)
                            if type in ("remote", "remote fwd"):
                                new_port = PortInfo.ForwardPort(
                                    PortInfo.REMOTE_TYPE,
                                    name,
                                    bind_ip,
                                    local_port,
                                    remote_host,
                                    remote_port)
                        elif type == "dynamic":
                            if not local_port:
                                self.errorReporter.show_error(
                                    "Invalid XML: Port missing attribute(s)")
                                return False
                            new_port = PortInfo.DynamicPort(
                                name,
                                bind_ip,
                                local_port)
                        else:
                            self.errorReporter.show_error(
                                "Invalid XML: invalid/missing port type")
                            return False
                        new_tunnel.add_port(new_port)
                # Done adding ports
                self.tunnelList.append(new_tunnel)

        # All Tunnel objects created successfully
        return True

    def convert_required_keys(self):
        """Convert required key settings from ID to filename."""
        for tunnel in self.tunnelList:
            if tunnel.requiredKey:
                key = self.lookup_key(tunnel.requiredKey)
                if key and tunnel.requiredKey != key.filename:
                    tunnel.requiredKey = key.filename

    def save_file(self, fileName=None):
        """Save to config file"""
        if not fileName:
            fileName = self.filename
        impl = minidom.getDOMImplementation()
        xml_document = impl.createDocument(None, "TunnelConfig", None)
        self.write_keys(xml_document)
        self.write_tunnels(xml_document)

        try:
            save_file = open(fileName, 'w')
            xml_document.documentElement.writexml(save_file,
                                                  addindent="  ",
                                                  newl="\n")
            save_file.close()
        except OSError as e:
            self.errorReporter.show_error(
                "Error saving tunnel config: (%(errno)d) %(errstr)s" % {'errno': e.errno, 'errstr': e.strerror})
            pass
        else:
            self.__m_filename = fileName
            self.__m_dirty = False

    def write_keys(self, document):
        """Write keys to the XML document"""
        self.keyList.sort()
        for key in self.keyList:
            keyElement = document.createElement("SSHKey")
            keyElement.setAttribute("filename", key.filename)
            document.documentElement.appendChild(keyElement)

    def write_tunnels(self, document):
        """Write all the tunnels to the XML document"""
        self.tunnelList.sort()
        for tunnel in self.tunnelList:
            tunnel.write(document)

    def clear(self):
        """Clear key and tunnel lists."""
        del self.tunnelList[:]
        del self.keyList[:]
        self.__m_dirty = False
        self.__m_filename = None

    def has_active_tunnel(self):
        """Check whether any tunnels in the list are active."""
        for tunnel in self.tunnelList:
            if tunnel.isActive:
                return True

        return False

    def insert_tunnel(self, tunnel):
        """Add a tunnel to the list."""
        self.__m_dirty = True
        self.tunnelList.append(tunnel)

    def insert_key(self, key):
        """Add a key to the list."""
        self.__m_dirty = True
        self.keyList.append(key)

    def remove_tunnel(self, tunnel):
        """Remove a tunnel from the list."""
        try:
            self.tunnelList.remove(tunnel)
            self.__m_dirty = True
        except ValueError as e:
            self.errorReporter.show_error("Error removing tunnel %(tun)s = %(err)s" % {'tun': tunnel, 'err': e})
            pass

    def remove_key(self, key):
        """Remove a key from the list."""
        try:
            self.keyList.remove(key)
            self.__m_dirty = True
        except ValueError as e:
            self.errorReporter.show_error("Error removing key %(key)s = %(err)s" % {'key': key, 'err': e})
            pass

    def start_tunnels(self, scheduleTunnelCleanup, autoStart=False):
        """Start all tunnels; or just the ones marked for autoStart."""
        for tunnel in self.tunnelList:
            if not autoStart or tunnel.needs_activate():
                tunnel.cancel_activate()
                tunnel.start(scheduleTunnelCleanup)
                self.isDirty = (not autoStart)

    def start_load_keys(self):
        """Load all keys that aren't currently loaded."""
        sshCmd = ""
        for key in self.keyList:
            if not key.isLoaded():
                sshCmd += " " + key.filename
        if sshCmd == "":
            # No keys to load
            return None
        cmd = ["/usr/bin/ssh-add </dev/null" + sshCmd]
        proc = subprocess.Popen(cmd, shell=True,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
        self.__m_sshAddRunning = True
        return proc

    def check_load_keys_complete(self, process, waitForCompletion=False):
        """Check ssh-add for completion, and cleanup if done."""
        if not self.isSSHAddRunning:
            # Already done; no need to wait
            return False
        elif waitForCompletion:
            process.wait()
        elif process.poll() is None:
            # True means to continue waiting
            return True
        if process.returncode != 0:
            print(str(process.stdout.read(), 'utf-8'), file=sys.stdout)
            print(str(process.stderr.read(), 'utf-8'), file=sys.stderr)
        else:
            self.update_keys_from_agent()
        self.__m_sshAddRunning = False
        # False means to stop waiting
        return False

    def update_keys_from_agent(self):
        """Keys were loaded. Update key data from the agent."""
        pipe = subprocess.Popen(["/usr/bin/ssh-add", "-l"],
                                stdout=subprocess.PIPE)
        stdout = pipe.communicate()[0]
        if pipe.returncode == 0:
            for l in stdout.splitlines():
                line = str(l, 'utf-8')
                fields = line.split()
                key = self.lookup_key(fields[2])
                if key:
                    key.set_attributes_from_agent(fields)

    def stop_tunnels(self):
        """Stop all tunnels."""
        for tunnel in self.tunnelList:
            if tunnel.isActive:
                tunnel.stop()

    def unload_keys(self):
        """Unload all keys."""
        subprocess.call(["/usr/bin/ssh-add", "-D"])

    def lookup_tunnel(self, name):
        """Lookup the named tunnel."""
        for tunnel in self.tunnelList:
            if tunnel.name == name:
                return tunnel
        return None

    def lookup_key(self, term):
        """Use term to lookup the key by fingerprint, filename, or id."""
        for key in self.keyList:
            if (key.filename == term or
                    key.id == term or
                    key.fingerprint == term):
                return key

        return None

    def default_route_exists(self):
        """Check whether there's a default route."""
        pipe = subprocess.Popen([self.__m_ipcmdpath, "route", "show"],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        stdout = pipe.communicate()[0]
        if pipe.returncode == 0:
            for l in stdout.splitlines():
                line = str(l, 'utf-8')
                fields = line.split()
                if len(fields) >= 3:
                    if fields[0] == "default" and fields[1] == "via":
                        ret = subprocess.call(
                            self.__m_ipcmdpath + " -s route get " +
                            fields[2] + " >/dev/null 2>&1", shell=True)
                        if ret == 0:
                            return True
                        return False
        return False
