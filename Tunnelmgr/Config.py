# Copyright (c) 2008-2013 Brandon williams
#
# AUTHOR:
# Brandon Williams <opensource@subakutty.net>
#
# This file is part of TunnelManager
#
# TunnelManager is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2, as published by
# the Free Software Foundation.
#
# TunnelManager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Tunnel manager; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#


import os
import sys
from configparser import RawConfigParser


class Configuration(object):
    """Configuration object to represent the properties file."""

    def __get_filename(self):
        return self.__m_filename

    filename = property(__get_filename)

    def __get_properties(self):
        return self.__m_properties

    properties = property(__get_properties)

    def __init__(self, filename=None):
        """Object constructor."""
        if not filename:
            if 'XDG_CONFIG_HOME' in os.environ:
                config_home = os.environ['XDG_CONFIG_HOME']
            else:
                config_home = os.path.join(os.path.expanduser('~'), '.config')
            self.configpath = config_home +  "/tunnelmgr"
            if not os.path.isdir(self.configpath):
                try:
                    os.mkdir(self.configpath)
                except OSError as e:
                    print("Failed creating directory %s: %s" % (self.configpath, e.strerror), file=sys.stderr)
                    pass
            filename = self.configpath + "/properties.conf"

        self.__m_filename = filename
        self.__m_properties = RawConfigParser()

        if os.path.exists(filename):
            self.properties.read(filename)

    def safe_set(self, section, option, value):
        """Safely set an option value, adding the section if necessary."""
        if section != 'DEFAULT' and not self.properties.has_section(section):
            self.properties.add_section(section)
        self.properties.set(section, option, value)

    def safe_get(self, section, option):
        """Safely get an option, reading from DEFAULT if section missing."""
        if section != 'DEFAULT' and not self.properties.has_section(section):
            section = 'DEFAULT'
        return self.properties.get(section, option)

    def safe_get_boolean(self, section, option):
        """Safely get an option, reading from DEFAULT if section missing."""
        if section != 'DEFAULT' and not self.properties.has_section(section):
            section = 'DEFAULT'
        return self.properties.getboolean(section, option)

    def write(self):
        """Write the properties file."""
        config_file = open(self.filename, 'w')
        self.properties.write(config_file)
