install:
ifeq ($(whoammi)),"root")
	echo "installing this program as user is recommended"
else
	./setup.py bdist_wheel
	pip3 install ./dist/tunnelmgr-0.0.0.2-py3-none-any.whl --user
endif
uninstall:
	pip3 uninstall tunnelmgr
clean:
	rm -rf ./dist/ ./build/ ./tunnelmgr.egg-info/
